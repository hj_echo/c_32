#ifndef __OPERATOR_H__
#define __OPERATOR_H__
#define _CRT_SECURE_NO_DEPRECATE
#pragma warning(disable:4996)
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#define MAXN 100

/****************账户链表结点定义****************/
typedef struct account *ptr_account;
typedef struct account
{
	char id[MAXN];
	char password[MAXN];
	int rank;		//1表示管理员（仅限管理员自行添加），2表示普通用户（可以注册）
	ptr_account next;
}account;
/***********************************************/

/****************信息链表结点定义****************/
typedef struct info *ptr_info;
typedef struct info
{
	char name[MAXN];
	int id;
	int course_id;
	double score;
	ptr_info next;
}info;
/***********************************************/

void system_init(ptr_account *phead, ptr_account *p);//初始化

void creat_acc_list(ptr_account *phead, ptr_account *p);//创建账户链表

void creat_info_list(ptr_info *phead);//创建信息链表

void input_password(char *str);

void registers(ptr_account *phead, ptr_account *p);//注册

void login(ptr_account *phead);//登录

void close(ptr_account *phead);//注销

void search_by_all(ptr_info phead);//查看全部信息

void search_by_name(ptr_info phead);//按名查找

void search_by_id(ptr_info phead);//按id查找

void add_info(ptr_info *phead);//添加学生信息

void change_info(ptr_info *phead);

void delete_info(ptr_info *phead);

void add_acc(ptr_account *phead, ptr_account *pcur);

void change_acc(ptr_account *phead);

void admin_close(ptr_account *phead);

void search_account(ptr_account phead);

#endif 