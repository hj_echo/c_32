#include "operator.h"
extern int flag1, flag2;

/******************初始化*******************/
void system_init(ptr_account *phead, ptr_account *p)
{
	FILE *fp1, *fp2, *fp3;
	fp1 = fopen("Configure.txt", "r+");
	char str1[MAXN]; char str2[MAXN];
	memset(str1, 0, sizeof(str1));
	memset(str2, 0, sizeof(str2));
	fscanf(fp1, "%s %s", str1, str2);
	fp2 = fopen(str1, "w+");
	fp3 = fopen(str2, "w+");
	fputs("hj 123456 1\n", fp2);
	*phead = (ptr_account)malloc(sizeof(account));
	ptr_account ptmp = (ptr_account)malloc(sizeof(account));
	ptmp->next = NULL;
	strcpy(ptmp->id, "hj");
	strcpy(ptmp->password, "123456");
	ptmp->rank = 1;
	(*phead)->next = ptmp;
	(*p) = ptmp;
	fclose(fp1); fclose(fp2); fclose(fp3);
}
/*******************************************/

/*******************创建账户链表*********************/
void creat_acc_list(ptr_account *phead, ptr_account *p)
{
	(*phead) = (ptr_account)malloc(sizeof(account));
	(*phead)->next = NULL;
	(*p) = (*phead);
	FILE *fp;
	fp = fopen("usr_account.txt", "r");
	while (1) {
		ptr_account tmp = (ptr_account)malloc(sizeof(account));
		tmp->next = NULL;
		if (3 != fscanf(fp, "%s %s %d", tmp->id, tmp->password, &tmp->rank)) {
			free(tmp);
			break;
		}
		(*p)->next = tmp;
		(*p) = tmp;
	}
	fclose(fp);
}
/*************************************************/

/****************创建信息链表并排序*********************/
void creat_info_list(ptr_info *phead)
{
	(*phead) = (ptr_info)malloc(sizeof(info));
	(*phead)->next = NULL;
	ptr_info q;
	FILE *fp;
	fp = fopen("usr_info.txt", "r");
	while (1) {
		q = (*phead);
		ptr_info tmp = (ptr_info)malloc(sizeof(info));
		tmp->next = NULL;
		if (4 != fscanf(fp, "%s %d %d %lf", tmp->name, &tmp->id, &tmp->course_id, &tmp->score)) {
			free(tmp);
			break;
		}
		if (q->next == NULL)
			q->next = tmp;
		else {
			while (q->next&&q->next->id < tmp->id)
				q = q->next;
			tmp->next = q->next;
			q->next = tmp;
		}
	}
	fclose(fp);
}
/*************************************************/

/*******************输入密码**************************/
void input_password(char *str)
{
	char c;
	int i = 0;
	while (1) {
		c = getch();
		if (c == '\r') {
			str[i] = '\0';
			printf("\n");
			break;
		}
		if (c == '\b') {
			printf("\b \b");
			i--;
			continue;
		}
		str[i++] = c;
		printf("*");
	}
}
/***********************************************/

/******************注册*********************/
void registers(ptr_account *phead, ptr_account *p)
{
	FILE *fp;
	fp = fopen("usr_account.txt", "a+");
	printf("欢迎来到注册界面\n");
	ptr_account tmp = (ptr_account)malloc(sizeof(account));
	tmp->rank = 2;
	tmp->next = NULL;
	ptr_account cur = (*phead)->next;
	printf("请输入账号\n");
	scanf("%s", tmp->id);
	while (1) {
		if (strcmp(tmp->id, cur->id)) {
			if (cur->next) {
				cur = cur->next;
			}
			else {
				break;
			}
		}
		else {
			printf("此用户已经存在，请重新注册!\n");
			fclose(fp);
			Sleep(1000);
			return;
		}
	}
	printf("请输入密码：\n");
	input_password(tmp->password);
	char str[MAXN];
	printf("请确认密码：\n");
	input_password(str);
	while (1) {
		if (!strcmp(tmp->password, str)) {
			memset(str, 0, sizeof(str));
			strcat(str, tmp->id);
			strcat(str, " ");
			strcat(str, tmp->password);
			fputs(str, fp);
			fputs(" 2\n", fp);
			fclose(fp);
			(*p)->next = tmp;
			(*p) = tmp;
			printf("账号注册成功，请登录！\n");
			flag1 = 1;
			Sleep(1000);
			return;
		}
		else {
			printf("两次密码不匹配！请重新输入！\n");
			input_password(tmp->password);
			printf("请确认密码\n");
			input_password(str);
		}
	}
}
/*****************************************************/

/*********************登录***************************/
void login(ptr_account *phead)
{
	printf("欢迎登陆！\n");
	ptr_account cur = (*phead)->next;
	account a; a.rank = 0;
	printf("输入账号\n");
	scanf("%s", a.id);
	while (1) {
		if (!strcmp(a.id, cur->id)) {
			a.rank = cur->rank;
			break;
		}
		else {
			if (cur->next) {
				cur = cur->next;
			}
			else {
				printf("此用户名不存在，请重新登陆！\n");
				Sleep(1000);
				return;
			}
		}
	}
	printf("请输入密码\n");
	input_password(a.password);
	int cnt = 2;
	while (cnt) {
		if (!strcmp(a.password, cur->password)) {
			printf("登录成功\n");
			if (a.rank == 1)
				flag2 = 2;
			else
				flag2 = 1;
			Sleep(1000);
			return;
		}
		else {
			printf("密码不正确，请重新输入！\n");
			Sleep(1000);
			printf("请输入密码\n");
			input_password(a.password);
			cnt--;
		}
	}
	printf("系统已锁定，请在30秒后重新登录！\n");
	Sleep(30 * 1000);
	return;
}
/***************************************************/

/**********************注销*************************/
void close(ptr_account *phead)
{
	FILE *fp, *ft;
	account tmp; tmp.rank = 0;
	printf("请输入要删除的账户名：");
	scanf("%s", tmp.id);
	ptr_account cur = (*phead);
	while (cur->next&&strcmp(cur->next->id, tmp.id)) {
		cur = cur->next;
	}
	if (cur->next == NULL && strcmp(cur->id, tmp.id)) {
		system("cls");
		printf("该用户不存在");
		Sleep(1000);
		return;
	}
	printf("请输入密码：");
	input_password(tmp.password);
	while (strcmp(tmp.password, "*tc*") && strcmp(tmp.password, cur->next->password)) {
		printf("密码错误，请重新密码或输入*tc*退出注销：\n");
		input_password(tmp.password);
	}
	if (!strcmp(tmp.password, "*tc*"))
		return;
	tmp.rank = cur->next->rank;
	fp = fopen("usr_account.txt", "a+");
	ft = fopen("tmp.txt", "w+");
	account ttmp;
	int index = 0;
	while (!feof(fp)) {
		fscanf(fp, "%s %s %d", ttmp.id, ttmp.password, &ttmp.rank);
		if (strcmp(ttmp.id, tmp.id)) {
			fprintf(ft, "%s %s %d\n", ttmp.id, ttmp.password, ttmp.rank);
		}
	}
	fclose(fp);
	fclose(ft);
	remove("usr_account.txt");
	rename("tmp.txt", "usr_account.txt");
	ptr_account q = cur->next;
	cur->next = q->next;
	free(q);
	printf("注销成功!");
	Sleep(1000);
}
/***************************************************/

/********************按名查找*********************/
void search_by_name(ptr_info phead)
{
	printf("Please input the name:");
	char name[MAXN];
	scanf("%s", name);
	ptr_info p = phead->next;
	while (p) {
		if (strcmp(p->name, name) == 0) {
			printf("Having found the information:\n");
			printf("id:%d  name:%s  course_id:%d  score:%.2lf\n", p->id, p->name, p->course_id, p->score);
			return;
		}
		p = p->next;
	}
	printf("Not found\n");
}
/*************************************************/

/********************按学号查找*********************/
void search_by_id(ptr_info phead)
{
	printf("Please input the name:");
	int id;
	scanf("%d", &id);
	ptr_info p = phead->next;
	while (p) {
		if (p->id == id) {
			printf("Having found the information:\n");
			printf("id:%d  name:%s  course_id:%d  score:%.2lf\n", p->id, p->name, p->course_id, p->score);
			return;
		}
		p = p->next;
	}
	printf("Not found\n");
}
/*************************************************/

/********************全表查找*********************/
void search_by_all(ptr_info phead)
{
	ptr_info p = phead->next;
	if (p == NULL) {
		printf("Not found\n");
		return;
	}
	printf("All information is as follows:\n");
	printf("     id          name          course_if          score\n");
	while (p) {
		printf("  %05d%14s%19d%15.2lf\n", p->id, p->name, p->course_id, p->score);
		p = p->next;
	}
}
/*************************************************/

/************************添加学生信息*****************************/
void add_info(ptr_info *phead)
{
	ptr_info tmp = (ptr_info)malloc(sizeof(info));
	tmp->next = NULL;
Inputid:
	system("cls");
	printf("Please input the id:");
	scanf("%d", &tmp->id);
	ptr_info p = (*phead)->next;
	while (p) {
		if (p->id == tmp->id) {
			printf("The id already exists\n");
			Sleep(1000);
			goto Inputid;
		}
		p = p->next;
	}
	printf("Please input the name:");
	scanf("%s", tmp->name);
	printf("Please input the id of the course:");
	scanf("%d", &tmp->course_id);
	printf("Please input the score:");
	scanf("%lf", &tmp->score);

	//写入链表
	p = (*phead);
	while (p->next&&p->next->id < tmp->id)
		p = p->next;
	tmp->next = p->next;
	p->next = tmp;

	//写入文件
	FILE *fp;
	fp = fopen("usr_info.txt", "a+");
	fprintf(fp, "%s %d %d %.2lf\n", tmp->name, tmp->id, tmp->course_id, tmp->score);
	fclose(fp);

	printf("Add information successfully！");
	Sleep(1000);
}
/*****************************************************************/

/************************更改学生信息*****************************/
void change_info(ptr_info *phead)
{
	FILE *fp, *ft;
	ptr_info tmp = (ptr_info)malloc(sizeof(info));
	tmp->next = NULL;
	printf("请输入要更改学生信息的学号：");
	scanf("%d", &tmp->id);
	ptr_info cur = (*phead);
	while (cur->next && tmp->id != cur->next->id) {
		cur = cur->next;
	}
	if (cur->next == NULL && tmp->id != cur->id) {
		system("cls");
		printf("该信息不存在");
		Sleep(1000);
		return;
	}
	strcpy(tmp->name, cur->next->name);
	tmp->course_id = cur->next->course_id;
	tmp->score = cur->next->score;
	char order[MAXN];
Change:
	printf("请输入更改信息类型：");
	printf("（输入1：学号；输入2：姓名；输入3：科目id；输入4：成绩）");
	scanf("%s", order);
	switch (atoi(order)) {
	case 1:
		printf("请输入学号：");
		scanf("%d", &tmp->id);
		break;
	case 2:
		printf("请输入姓名：");
		scanf("%s", tmp->name);
		break;
	case 3:
		printf("请输入科目id：");
		scanf("%d", &tmp->course_id);
		break;
	case 4:
		printf("请输入成绩：");
		scanf("%lf", &tmp->score);
		break;
	default:
		printf("指令错误，请重新输入!");
		Sleep(1000);
		system("cls");
		goto Change;
	}
	fp = fopen("usr_info.txt", "a+");
	ft = fopen("tmp.txt", "w+");
	info ttmp;
	int index = 0;
	while (4 == fscanf(fp, "%s %d %d %lf", ttmp.name, &ttmp.id, &ttmp.course_id, &ttmp.score)) {
		if (ttmp.id == tmp->id && (ttmp.course_id != tmp->course_id || ttmp.score != tmp->score || strcmp(ttmp.name, tmp->name))) {
			fprintf(ft, "%s %d %d %.2lf\n", tmp->name, tmp->id, tmp->course_id, tmp->score);
		}
		else if (ttmp.id != tmp->id && ttmp.course_id == tmp->course_id && ttmp.score == tmp->score && strcmp(ttmp.name, tmp->name) == 0) {
			fprintf(ft, "%s %d %d %.2lf\n", tmp->name, tmp->id, tmp->course_id, tmp->score);
		}
		else {
			fprintf(ft, "%s %d %d %.2lf\n", ttmp.name, ttmp.id, ttmp.course_id, ttmp.score);
		}
	}
	fclose(fp);
	fclose(ft);
	remove("usr_info.txt");
	rename("tmp.txt", "usr_info.txt");
	ptr_info q = cur->next;
	tmp->next = q->next;
	cur->next = tmp;
	free(q);
	printf("更改成功!");
	Sleep(1000);
}
/*****************************************************************/

/************************删除学生信息*****************************/
void delete_info(ptr_info *phead)
{
	FILE *fp, *ft;
	info tmp;
	tmp.id = 0; tmp.course_id = 0; tmp.score = 0.0;
	printf("请输入要删除学生信息的学号：");
	scanf("%d", &tmp.id);
	ptr_info cur = (*phead);
	while (cur->next && tmp.id != cur->next->id) {
		cur = cur->next;
	}
	if (cur->next == NULL && tmp.id != cur->id) {
		system("cls");
		printf("该信息不存在");
		Sleep(1000);
		return;
	}
	printf("真的要删除该条信息吗？\n");
	printf("按Y确认/按N退出：");
	char c;
	while (1) {
		rewind(stdin);
		scanf("%c", &c);
		if (c == 'n' || c == 'N') {
			return;
		}
		else if (c == 'y' || c == 'Y') {
			break;
		}
		else {
			printf("请重新输入指令：");
			continue;
		}
	}
	fp = fopen("usr_info.txt", "a+");
	ft = fopen("tmp.txt", "w+");
	info ttmp;
	int index = 0;
	while (!feof(fp)) {
		fscanf(fp, "%s %d %d %lf", ttmp.name, &ttmp.id, &ttmp.course_id, &ttmp.score);
		if (ttmp.id != tmp.id) {
			fprintf(fp, "%s %d %d %.2lf", ttmp.name, ttmp.id, ttmp.course_id, ttmp.score);
		}
	}
	fclose(fp);
	fclose(ft);
	remove("usr_info.txt");
	rename("tmp.txt", "usr_info.txt");
	ptr_info q = cur->next;
	cur->next = q->next;
	free(q);
	printf("删除成功!");
	Sleep(1000);
}
/*****************************************************************/

/************************添加账户信息*****************************/
void add_acc(ptr_account *phead, ptr_account *pcur)
{
	ptr_account tmp = (ptr_account)malloc(sizeof(account));
	tmp->next = NULL;
Inputid:
	system("cls");
	printf("Please input the id:");
	scanf("%s", tmp->id);
	ptr_account p = (*phead)->next;
	while (p) {
		if (p->id == tmp->id) {
			printf("The id already exists\n");
			Sleep(1000);
			goto Inputid;
		}
		p = p->next;
	}
	printf("Please input the password:");
	scanf("%s", tmp->password);
	printf("Please input the rank:");
	scanf("%d", &tmp->rank);

	//写入链表
	(*pcur)->next = tmp;
	(*pcur) = tmp;

	//写入文件
	FILE *fp;
	fp = fopen("usr_account.txt", "a+");
	fprintf(fp, "%s %s %d\n", tmp->id, tmp->password, tmp->rank);
	fclose(fp);

	printf("Add account successfully！");
	Sleep(1000);
}
/*****************************************************************/

/************************更改账户信息*****************************/
void change_acc(ptr_account *phead)
{
	FILE *fp, *ft;
	ptr_account tmp = (ptr_account)malloc(sizeof(account));
	tmp->next = NULL;
	printf("请输入要更改账户的用户名：");
	scanf("%s", tmp->id);
	ptr_account cur = (*phead);
	while (cur->next && strcmp(tmp->id, cur->next->id)) {
		cur = cur->next;
	}
	if (cur->next == NULL && strcmp(tmp->id, cur->next->id)) {
		system("cls");
		printf("该账户不存在");
		Sleep(1000);
		return;
	}
	strcpy(tmp->id, cur->next->id);
	strcpy(tmp->password, cur->next->password);
	tmp->rank = cur->next->rank;
	char order[MAXN];
Change:
	printf("请输入更改的账户信息类型：");
	printf("（输入1：用户名；输入2：密码；输入3：访问等级）");
	scanf("%s", order);
	switch (atoi(order)) {
	case 1:
		printf("请输入用户名：");
		scanf("%s", tmp->id);
		break;
	case 2:
		printf("请输入密码：");
		scanf("%s", tmp->password);
		break;
	case 3:
		while (1) {
			printf("请输入访问等级（1或2）：");
			scanf("%d", &tmp->rank);
			if (tmp->rank == 1 || tmp->rank == 2)
				break;
			printf("输入非法，请重新输入！\n");
			Sleep(1000);
		}
		break;
	default:
		printf("指令错误，请重新输入!");
		Sleep(1000);
		system("cls");
		goto Change;
	}
	fp = fopen("usr_account.txt", "a+");
	ft = fopen("tmp.txt", "w+");
	account ttmp;
	int index = 0;
	while (3 == fscanf(fp, "%s %s %d", ttmp.id, ttmp.password, &ttmp.rank)) {
		if (strcmp(ttmp.id, tmp->id) == 0 && (strcmp(ttmp.password, tmp->password) || ttmp.rank != tmp->rank)) {
			fprintf(ft, "%s %s %d\n", tmp->id, tmp->password, tmp->rank);
		}
		else if (strcmp(ttmp.id, tmp->id) && strcmp(ttmp.password, tmp->password) == 0 && ttmp.rank == tmp->rank) {
			fprintf(ft, "%s %s %d\n", tmp->id, tmp->password, tmp->rank);
		}
		else {
			fprintf(ft, "%s %s %d\n", ttmp.id, ttmp.password, ttmp.rank);
		}
	}
	fclose(fp);
	fclose(ft);
	remove("usr_account.txt");
	rename("tmp.txt", "usr_account.txt");
	ptr_account q = cur->next;
	tmp->next = q->next;
	cur->next = tmp;
	free(q);
	printf("更改成功!");
	Sleep(1000);
}
/*****************************************************************/

/************************删除账户信息*****************************/
void admin_close(ptr_account *phead)
{
	FILE *fp, *ft;
	account tmp; tmp.rank = 0;
	printf("请输入要删除的账户名：");
	scanf("%s", tmp.id);
	ptr_account cur = (*phead);
	while (cur->next&&strcmp(cur->next->id, tmp.id)) {
		cur = cur->next;
	}
	if (cur->next == NULL && strcmp(cur->id, tmp.id)) {
		system("cls");
		printf("该用户不存在");
		Sleep(1000);
		return;
	}
	printf("真的要删除该用户吗？\n");
	printf("按Y确认/按N退出：");
	char c;
	while (1) {
		rewind(stdin);
		scanf("%c", &c);
		if (c == 'n' || c == 'N') {
			return;
		}
		else if (c == 'y' || c == 'Y') {
			break;
		}
		else {
			printf("请重新输入指令：");
			continue;
		}
	}
	fp = fopen("usr_account.txt", "a+");
	ft = fopen("tmp.txt", "w+");
	account ttmp;
	int index = 0;
	while (!feof(fp)) {
		fscanf(fp, "%s %s %d", ttmp.id, ttmp.password, &ttmp.rank);
		if (strcmp(ttmp.id, tmp.id)) {
			fprintf(ft, "%s %s %d\n", ttmp.id, ttmp.password, ttmp.rank);
		}
	}
	fclose(fp);
	fclose(ft);
	remove("usr_account.txt");
	rename("tmp.txt", "usr_account.txt");
	ptr_account q = cur->next;
	cur->next = q->next;
	free(q);
	printf("删除成功!");
	Sleep(1000);
}
/*****************************************************************/

/************************查看账户信息*****************************/
void search_account(ptr_account phead)
{
	ptr_account p = phead->next;
	if (p == NULL) {
		printf("Not found\n");
		return;
	}
	printf("All accounts are as follows:\n");
	printf("          id          password          rank\n");
	while (p) {
		printf("%12s%18s%14d\n", p->id, p->password, p->rank);
		p = p->next;
	}
}
/*****************************************************************/
