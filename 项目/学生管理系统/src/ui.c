#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>

void StartWindow()
{
	printf(":***************************************************************:\n");
	printf(":**********Student   Information   Management   System**********:\n");
	printf(":***************************************************************:\n");
	printf(":                                                               :\n");
	printf(":               1.register  an  account                         :\n");
	printf(":               2.login   your  account                         :\n");
	printf(":               3.close         account                         :\n");
	printf(":               4.exit                                          :\n");
	printf(":                                                               :\n");
	printf(":***************************************************************:\n\n");
}

void AdminMainWindow()
{
	printf(":***************************************************************:\n");
	printf(":**********Student   Information   Management   System**********:\n");
	printf(":****************************( Administrator  Version )*********:\n");
	printf(":***************************************************************:\n");
	printf(":                                                               :\n");
	printf(":              1.search   student   information                 :\n");
	printf(":              2. add     student   information                 :\n");
	printf(":              3.update   student   information                 :\n");
	printf(":              4.delete   student   information                 :\n");
	printf(":              5. add     user   account                        :\n");
	printf(":              6.update   user   account                        :\n");
	printf(":              7.delete   user   account                        :\n");
	printf(":              8.search   user   account                        :\n");
	printf(":              9.exit                                           :\n");
	printf(":                                                               :\n");
	printf(":***************************************************************:\n\n");
}

void NormalMainWindow()
{
	printf(":***************************************************************:\n");
	printf(":**********Student   Information   Management   System**********:\n");
	printf(":***********************************( Normal  Version )*********:\n");
	printf(":***************************************************************:\n");
	printf(":                                                               :\n");
	printf(":              1.search   student   information                 :\n");
	printf(":              9.exit                                           :\n");
	printf(":                                                               :\n");
	printf(":***************************************************************:\n\n");
}

void NormalUserSearchWindow()
{
	printf(":***************************************************************:\n");
	printf(":                                                               :\n");
	printf(":                  1.search   by   name                         :\n");
	printf(":                  2.search   by   id                           :\n");
	printf(":                  3.return                                     :\n");
	printf(":                                                               :\n");
	printf(":***************************************************************:\n");
}

void  AdministratorSearchWindow()
{
	printf(":***************************************************************:\n");
	printf(":                                                               :\n");
	printf(":                  1.search   all                               :\n");
	printf(":                  2.search   by   name                         :\n");
	printf(":                  3.search   by   id                           :\n");
	printf(":                  4.return                                     :\n");
	printf(":                                                               :\n");
	printf(":***************************************************************:\n");
}