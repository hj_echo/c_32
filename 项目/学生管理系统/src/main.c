#define _CRT_SECURE_NO_DEPRECATE
#include "operator.h"
#include "ui.h"
int flag1, flag2;

int main()
{
	char order[MAXN];
	ptr_account acc_list = NULL;
	ptr_account acc_cur = NULL;
	ptr_info info_list = NULL;
	FILE *fp;
	fp = fopen("usr_account.txt", "r");
	if (NULL == fp) {
		system_init(&acc_list, &acc_cur);
	}
	else {
		creat_acc_list(&acc_list, &acc_cur);
		fclose(fp);
	}
	creat_info_list(&info_list);
Start:
	system("cls");
	flag1 = 0;
	flag2 = 0;
	StartWindow();
Lable1:
	printf("select a number:");
	scanf("%s", order);
	switch (atoi(order)) {
	case 1:
		goto Register;
	case 2:
		goto Login;
	case 3:
		goto Close;
	case 4:
		goto End;
	default:
		printf("Input Error\n");
		goto Lable1;
	}
Register:
	system("cls");
	registers(&acc_list, &acc_cur);
	if (flag1 == 0)
		goto Start;
	else
		goto Login;
Login:
	system("cls");
	login(&acc_list);
	if (flag2 == 0)
		goto Start;
	else if (flag2 == 1)
		goto Main1;
	else
		goto Main2;
Close:
	system("cls");
	close(&acc_list);
	goto Start;
Main1:	//普通用户界面
	system("cls");
	NormalMainWindow();
Lable2:
	printf("select a number:");
	scanf("%s", order);
	switch (atoi(order)) {
	case 1:
		goto Search1;
	case 9:
		goto Start;
	default:
		printf("Input Error\n");
		goto Lable2;
	}
Main2:	//管理员用户界面
	system("cls");
	AdminMainWindow();
Lable3:
	printf("select a number:");
	scanf("%s", order);
	switch (atoi(order)) {
	case 1:
		goto Search2;
	case 2:
		goto AddStudent;
	case 3:
		goto UpdateStudent;
	case 4:
		goto DeleteStudent;
	case 5:
		goto AddUser;
	case 6:
		goto UpdateUser;
	case 7:
		goto DeleteUser;
	case 8:
		goto SearchUser;
	case 9:
		goto Start;
	default:
		printf("Input Error\n");
		goto Lable3;
	}
Search1:
	system("cls");
	NormalUserSearchWindow();
Lable4:
	printf("select a number:");
	scanf("%s", order);
	switch (atoi(order)) {
	case 1:
		system("cls");
		search_by_name(info_list);
		system("pause");
		goto Search1;
	case 2:
		system("cls");
		search_by_id(info_list);
		system("pause");
		goto Search1;
	case 3:
		goto Main1;
	default:
		printf("Input Error\n");
		goto Lable4;
	}
Search2:
	system("cls");
	AdministratorSearchWindow();
Lable6:
	printf("select a number:");
	scanf("%s", order);
	switch (atoi(order)) {
	case 1:
		system("cls");
		search_by_all(info_list);
		system("pause");
		goto Search2;
	case 2:
		system("cls");
		search_by_name(info_list);
		system("pause");
		goto Search2;
	case 3:
		system("cls");
		search_by_id(info_list);
		system("pause");
		goto Search2;
	case 4:
		goto Main2;
	default:
		printf("Input Error\n");
		goto Lable6;
	}
AddStudent:
	add_info(&info_list);
	goto Main2;
UpdateStudent:
	system("cls");
	change_info(&info_list);
	goto Main2;
DeleteStudent:
	system("cls");
	delete_info(&info_list);
	goto Main2;
AddUser:
	system("cls");
	add_acc(&acc_list, &acc_cur);
	goto Main2;
UpdateUser:
	system("cls");
	change_acc(&acc_list);
	goto Main2;
DeleteUser:
	system("cls");
	admin_close(&acc_list);
	goto Main2;
SearchUser:
	system("cls");
	search_account(acc_list);
	system("pause");
	goto Main2;
End:
	system("cls");
	return 0;
}