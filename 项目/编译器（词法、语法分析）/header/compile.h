#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <Windows.h>
#include <crtdbg.h>


#define MAXKEY 1024//哈希表表长


/***********用到的数据结构***********/
//单词编码
enum e_TokenCode {
	/* 运算符及分隔符 */
	TK_PLUS, // + 加号
	TK_MINUS, // - 减号
	TK_STAR, // * 星号
	TK_DIVIDE, // / 除号
	TK_MOD, // % 求余运算符
	TK_EQ, // == 等于号
	TK_NEQ, // != 不等于号
	TK_LT, // < 小于号
	TK_LEQ, // <= 小于等于号
	TK_GT, // > 大于号
	TK_GEQ, // >= 大于等于号
	TK_ASSIGN, // = 赋值运算符
	TK_POINTSTO, // -> 指向结构体成员运算符
	TK_DOT, // . 结构体成员运算符
	TK_AND, // & 地址与运算符
	TK_OPENPA, // ( 左圆括号
	TK_CLOSEPA, // ) 右圆括号
	TK_OPENBR, // [ 左中括号
	TK_CLOSEBR, // ] 右中括号
	TK_BEGIN, // { 左大括号
	TK_END, // } 右大括号
	TK_SEMICOLON, // ; 分号
	TK_COMMA, // , 逗号
	TK_ELLIPSIS, // ... 省略号
	TK_EOF, // 文件结束符
	/* 常量 */
	TK_CINT, // 整型常量
	TK_CDOUBLE,//浮点型常量
	TK_CCHAR, // 字符常量
	TK_CSTR, // 字符串常量
	/* 关键字 */
	KW_CHAR, // char关键字
	KW_SHORT, // short关键字
	KW_INT, // int关键字
	KW_DOUBLE, // double关键字
	KW_VOID, // void关键字
	KW_STRUCT, // struct关键字
	KW_IF, // if关键字
	KW_ELSE, // else关键字
	KW_FOR, // for关键字
	KW_CONTINUE, // continue关键字
	KW_BREAK, // break关键字
	KW_RETURN, // return关键字
	KW_SIZEOF, // sizeof关键字
	KW_ALIGN, // __align关键字
	KW_CDECL, // __cdecl关键字 standard c call
	KW_STDCALL, // __stdcall关键字 pascal c call
	/* 标识符 */
	TK_IDENT //函数
};//为了让项目不过于复杂，未考虑逻辑与，逻辑或，逻辑非等逻辑运算符，自增自减运算符不考虑，但原理完全一致

//动态字符串
typedef struct DynString {
	int cnt;
	int cap;
	char *data;
}DynString;
void dstring_init(DynString*, int);
void dstring_free(DynString*);
void dstring_reset(DynString*);
void dstring_realloc(DynString*, int);
void dstring_getchar(DynString*, char);

//动态数组
typedef struct DynArray {
	int cnt;
	int cap;
	void **data;//采用指针数组，可以使移动元素时只移动指针，开销小
}DynArray;
void darray_init(DynArray*, int);
void darray_free(DynArray*);
void darray_reset(DynArray*);
void darray_pushback(DynArray*, void*);
void darray_realloc(DynArray*, int);

//单词表
typedef struct TkWord {
	int tkcode;
	struct TkWord *next;
	char *word;
}TkWord;
int elf_hash(char*);
TkWord* tkword_insert(TkWord*);
TkWord* tkword_find(char*);
TkWord* tkword_find_insert(char*);
/************************************/


/***********错误处理***********/
enum  e_ErrorLevel {//错误等级
	LEVEL_WARNING,
	LEVEL_ERROR
};
enum e_WorkingStage {//工作阶段
	STAGE_COMPILE,
	STAGE_LINK
};
void handle_exception(int, int, char*, va_list);
void warning(char*, ...);
void error(char*, ...);
void expect(char*);
/******************************/


/***********词法分析***********/
void lex_anaylysis_init();
void ignore_space();
void parse_comment();
void read_first();
int is_alp(char);
int is_digit(char);
TkWord* parse_identifier();
void parse_num();
void parse_string(char);
void get_token();
/******************************/


/***********语法分析***********/
enum e_SynTaxState {//语法状态
	SNTX_NUL,       // 空状态，没有语法缩进动作
	SNTX_SP,		// 空格 int a; int __stdcall MessageBoxA(); return 1;
	SNTX_LF_HT,		// 换行并缩进，每一个声明、函数定义、语句结束都要置为此状态
	SNTX_DELAY      // 延迟取出下一单词后确定输出格式，取出下一个单词后，根据单词类型单独调用syntax_indent确定格式进行输出 
};

enum e_StorageClass {//存储类型
	SC_GLOBAL = 0x00f0, //包括整型常量、字符常量、字符串常量、全局变量、函数定义
	SC_LOCAL = 0x00f1, //栈中变量
	SC_LLOCAL = 0x00f2, //寄存器溢出存放栈中
	SC_CMP = 0x00f3, //使用标志寄存器
	SC_VALMASK = 0x00ff, //存储类型掩码
	SC_LVAL = 0x0100, //左值
	SC_SYM = 0x0200, //符号
	SC_ANOM = 0x10000000, //匿名符号
	SC_STRUCT = 0x20000000, //结构体符号
	SC_MEMBER = 0x40000000, //结构成员变量
	SC_PARAMS = 0x80000000, //函数参数
};

void gram_get_token();
void skip(int);
int is_type_specifier(int);

void print_tab(int);
void syntax_indent();

void translation_unit();
void external_declaration(int);
int type_specifier();
void struct_specifier();
void struct_declaration_list();
void struct_declaration();
void function_calling_convention(int*);
void struct_member_alignment();
void declarator();
void direct_declarator();
void direct_declaration_postfix();
void parameter_type_list();
void funcbody();
void initializer();

void statement();
void compound_statement();
void expression_statement();
void if_statement();
void for_statement();
void break_statement();
void continue_statement();
void return_statement();

void expression();
void assignment_expression();
void equality_expression();
void relational_expression();
void additive_expression();
void multiplicative_expression();
void unary_expression();
void sizeof_expression();
void postfix_expression();
void primary_expression();
void argument_expression_list();
/******************************/


/***********控件***********/
void getch();
void print_line();
char* get_printword(int);
void color_token();
void init();
void cleanup();
void lex_program();
void grammar_program();
/**************************/


/***********全局变量声明***********/
extern FILE *fp;
extern char *filename;

extern DynArray print_warning;

extern TkWord *tk_hashtable[MAXKEY];
extern DynArray tk_table;

extern DynString str;
extern DynString sourcestr;

extern DynArray token_seq;
extern DynArray const_seq;

extern char ch;
extern int token;
extern int line_num;
extern int intvalue;
extern double doublevalue;
extern int numflag;

extern int syntax_state;
extern int syntax_level;
extern int token_tag;
extern int const_tag;
/*********************************/