#include "compile.h"

DynString str;//单词字符串
DynString sourcestr;//单词源码字符串

DynArray token_seq;//token序列表，用于按分析顺序存放词法分析出的token
DynArray const_seq;//常量序列表，用于按分析顺序存放词法分析出的常量

char ch;//当前读到的源码字符
int token;//当前识别到的单词的单词编码
int line_num;//行号
int intvalue;//当前识别到的单词代表的整形常量
double doublevalue;//当前识别到的单词代表的浮点型常量
int numflag;//判断数字常量是整形还是浮点型的标志

void lex_anaylysis_init() {//词法分析初始化
	static TkWord keywords[] = {//预输入关键字、运算符
	{ TK_PLUS,		NULL,	  "+" },
	{ TK_MINUS,		NULL,	  "-" },
	{ TK_STAR,		NULL,	  "*" },
	{ TK_DIVIDE,	NULL,	  "/" },
	{ TK_MOD,		NULL,	  "%" },
	{ TK_EQ,		NULL,	  "==" },
	{ TK_NEQ,		NULL,	  "!=" },
	{ TK_LT,		NULL,	  "<" },
	{ TK_LEQ,		NULL,	  "<=" },
	{ TK_GT,		NULL,	  ">" },
	{ TK_GEQ,		NULL,	  ">=" },
	{ TK_ASSIGN,	NULL,	  "=" },
	{ TK_POINTSTO,	NULL,	  "->" },
	{ TK_DOT,		NULL,	  "." },
	{ TK_AND,		NULL,	  "&" },
	{ TK_OPENPA,	NULL,	  "(" },
	{ TK_CLOSEPA,	NULL,	  ")" },
	{ TK_OPENBR,	NULL,	  "[" },
	{ TK_CLOSEBR,	NULL,	  "]" },
	{ TK_BEGIN,		NULL,	  "{" },
	{ TK_END,		NULL,	  "}" },
	{ TK_SEMICOLON,	NULL,	  ";" },
	{ TK_COMMA,		NULL,	  "," },
	{ TK_ELLIPSIS,	NULL,	"..." },
	{ TK_EOF,		NULL,	 "End_Of_File" },
	{ TK_CINT,		NULL,	 	"整型常量" },
	{ TK_CDOUBLE,	NULL,	 	"浮点型常量" },
	{ TK_CCHAR,		NULL,		"字符常量" },
	{ TK_CSTR,		NULL,		"字符串常量" },
	{ KW_CHAR,		NULL,		"char" },
	{ KW_SHORT,		NULL,		"short" },
	{ KW_INT,		NULL,		"int" },
	{ KW_DOUBLE,	NULL,		"double" },
	{ KW_VOID,		NULL,		"void" },
	{ KW_STRUCT,	NULL,		"struct" },
	{ KW_IF,		NULL,		"if" },
	{ KW_ELSE,		NULL,		"else" },
	{ KW_FOR,		NULL,		"for" },
	{ KW_CONTINUE,	NULL,		"continue" },
	{ KW_BREAK,		NULL,		"break" },
	{ KW_RETURN,	NULL,		"return" },
	{ KW_SIZEOF,	NULL,		"sizeof" },
	{ KW_ALIGN,		NULL,		"__align" },
	{ KW_CDECL,		NULL,		"__cdecl" },
	{ KW_STDCALL,	NULL,		"__stdcall" },
	{ 0,			NULL,		NULL }
	};
	TkWord *pword = &keywords[0];
	darray_init(&tk_table, 50);//初始化单词表
	while (pword->word) {
		tkword_insert(pword);//将输入的关键字和运算符插入单词哈希表和单词表
		pword++;
	}
	darray_init(&token_seq, 200);//初始化token序列表
	darray_init(&const_seq, 200);//初始化常量序列表
}

void ignore_space() {//忽略空格，tab和回车
	while (ch == ' ' || ch == '\t' || ch == '\r') {
		if (ch == '\r') {
			getch();
			if (ch != '\n') {
				return;
			}
			line_num++;
		}
		printf("%c", ch);
		if (ch == '\n') {
			print_line();
		}
		getch();//该字符处理完毕，读取下一个字符
	}
}

void parse_comment() {//解析并忽略注释
	if (ch == '/') {//处理"//"型注释
		getch();
		while (1) {
			if (ch == '\r' || ch == EOF) {
				break;
			}
			else {
				getch();
			}
		}
	}
	else {//处理“*”型注释
		getch();
		while (1) {//不停读取注释内容，直到找到注释结束符
			while (1) {
				if (ch == '\n' || ch == '*' || ch == EOF) {
					break;
				}
				else {
					getch();
				}
			}
			if (ch == '\n') {
				line_num++;
				getch();
			}
			else if (ch == '*') {
				getch();
				if (ch == '/') {
					getch();
					return;
				}
			}
			else {
				error("未找到配对的注释结束符");
				return;
			}
		}
	}
}

void read_first() {//处理单词的第一个字符，忽略分隔符以及注释
	while (1) {
		if (ch == ' ' || ch == '\t' || ch == '\r') {
			ignore_space();
		}
		else if (ch == '/') {
			getch();//向前多读一个字符猜测是否是注释开始符，猜错了就把多读的字符放回流中
			if (ch == '*' || ch == '/') {
				parse_comment();
			}
			else {
				ungetc(ch, fp);
				ch = '/';
				break;
			}
		}
		else {
			break;
		}
	}
}

int is_alp(char c) {//判断是否是字母
	return (c >= 'a'&&c <= 'z') || (c >= 'A'&&c <= 'Z');
}

int is_digit(char c) {//判断是否是数字
	return c >= '0' && c <= '9';
}

TkWord* parse_identifier() {//解析标识符
	dstring_reset(&str);
	dstring_getchar(&str, ch);
	getch();
	while (is_alp(ch) || is_digit(ch) || ch == '_') {
		dstring_getchar(&str, ch);
		getch();
	}
	dstring_getchar(&str, '\0');
	return tkword_find_insert(str.data);
}

void parse_num() {//解析数字常量
	dstring_reset(&str);
	dstring_reset(&sourcestr);
	do {
		dstring_getchar(&str, ch);
		dstring_getchar(&sourcestr, ch);
		getch();
	} while (is_digit(ch));
	if (ch == '.') {//有小数点，常数为浮点型
		numflag = 1;
		do {
			dstring_getchar(&str, ch);
			dstring_getchar(&sourcestr, ch);
			getch();
		} while (is_digit(ch));
	}
	dstring_getchar(&str, '\0');
	dstring_getchar(&sourcestr, '\0');
	if (numflag) {
		doublevalue = atof(str.data);
	}
	else {
		intvalue = atoi(str.data);
	}
}

void parse_string(char sep) {//sep为界符，sep为单引号解析字符常量，sep为双引号解析字符串常量
	char c;//用于存放转义字符
	dstring_reset(&str);
	dstring_reset(&sourcestr);
	dstring_getchar(&sourcestr, sep);//源码字符串记录带界符的串
	getch();
	while (1) {
		if (ch == sep) {//遇到界符，字符串识别结束
			break;
		}
		else if (ch == '\\') {//遇到左斜杠，识别转义字符
			dstring_getchar(&sourcestr, ch);//源码字符串记录输入的原字符
			getch();
			switch (ch) {
			case '0'://空字符
				c = '\0';
				break;
			case 'a'://响铃
				c = '\a';
				break;
			case 'b'://退格
				c = '\b';
				break;
			case 't'://横向制表
				c = '\t';
				break;
			case 'n'://换行
				c = '\n';
				break;
			case 'v'://纵向制表
				c = '\v';
				break;
			case 'f'://换页
				c = '\f';
				break;
			case 'r'://回车
				c = '\r';
				break;
			case '\"'://双引号
				c = '\"';
				break;
			case '\''://单引号
				c = '\'';
				break;
			case '\\'://左斜杠
				c = '\\';
				break;
			default://是非法的转义字符
				c = ch;
				if (c >= '!'&&c < '~')
					warning("非法转义字符：\'\\%c\'", c);
				else
					warning("非法转义字符：\'\\0x%x\'", c);
				break;
			}
			dstring_getchar(&str, c);//str中保存的是识别出的转义字符
			dstring_getchar(&sourcestr, ch);//sourcestr中保存的是源码输入的字符
			getch();
		}
		else {//其他字符
			dstring_getchar(&str, ch);
			dstring_getchar(&sourcestr, ch);
			getch();
		}
	}
	dstring_getchar(&str, '\0');
	dstring_getchar(&sourcestr, sep);//源码字符串记录带界符的串
	dstring_getchar(&sourcestr, '\0');
	getch();
}

void get_token() {//词法分析主程序，识别单词
	read_first();//读第一个字符
	if (is_alp(ch)||ch=='_') {//识别标识符
		TkWord *word;
		word = parse_identifier();
		token = word->tkcode;
	}
	else if (is_digit(ch)) {//识别数字常量
		parse_num();
		if (numflag) {
			token = TK_CDOUBLE;
		}
		else {
			token = TK_CINT;
		}
	}
	else {//识别运算符、分隔符、字符串常量
		switch (ch){
		case '+':
			getch();
			token = TK_PLUS;
			break;
		case '-':
			getch();
			if (ch == '>'){
				token = TK_POINTSTO;
				getch();
			}
			else {
				token = TK_MINUS;
			}
			break;
		case '/':
			token = TK_DIVIDE;
			getch();
			break;
		case '%':
			token = TK_MOD;
			getch();
			break;
		case '=':
			getch();
			if (ch == '='){
				token = TK_EQ;
				getch();
			}
			else {
				token = TK_ASSIGN;
			}
			break;
		case '!':
			getch();
			if (ch == '='){
				token = TK_NEQ;
				getch();
			}
			else {
				error("暂不支持逻辑运算符");
			}
			break;
		case '<':
			getch();
			if (ch == '='){
				token = TK_LEQ;
				getch();
			}
			else {
				token = TK_LT;
			}
			break;
		case '>':
			getch();
			if (ch == '='){
				token = TK_GEQ;
				getch();
			}
			else {
				token = TK_GT;
			}
			break;
		case '.':
			getch();
			if (ch == '.'){
				getch();
				if (ch != '.') {
					error("省略号拼写错误");
				}
				else {
					token = TK_ELLIPSIS;
				}
				getch();
			}
			else{
				token = TK_DOT;
			}
			break;
		case '&':
			getch();
			if (ch == '&') {
				error("暂不支持逻辑运算符");
			}
			else {
				token = TK_AND;
			}
			break;
		case ';':
			token = TK_SEMICOLON;
			getch();
			break;
		case ']':
			token = TK_CLOSEBR;
			getch();
			break;
		case '}':
			token = TK_END;
			getch();
			break;
		case ')':
			token = TK_CLOSEPA;
			getch();
			break;
		case '[':
			token = TK_OPENBR;
			getch();
			break;
		case '{':
			token = TK_BEGIN;
			getch();
			break;
		case ',':
			token = TK_COMMA;
			getch();
			break;
		case '(':
			token = TK_OPENPA;
			getch();
			break;
		case '*':
			token = TK_STAR;
			getch();
			break;
		case '\'':
			parse_string(ch);
			token = TK_CCHAR;
			intvalue = *(char *)str.data;//这里整形常量是字符常量代表的ASCII值
			break;
		case '\"':
			parse_string(ch);
			token = TK_CSTR;
			break;
		case EOF:
			token = TK_EOF;
			break;
		default:
			error("不认识的字符:\\x%02x", ch); //上面字符以外的字符，只允许出现在源码字符串，不允许出现的源码的其它位置
			getch();
			break;
		}
	}
	int *tmp_token=(int*)malloc(sizeof(int));
	*tmp_token = token;
	darray_pushback(&token_seq, tmp_token);
	if (token >= TK_CINT && token <= TK_CSTR) {
		char *tmp_const = (char*)malloc(strlen(sourcestr.data)+1);
		strcpy(tmp_const, sourcestr.data);
		darray_pushback(&const_seq, tmp_const);
	}
}