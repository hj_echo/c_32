#include "compile.h"

void dstring_init(DynString *dstr, int size) {//初始化
	dstr->cnt = 0;
	dstr->cap = size;
	dstr->data = (char*)malloc(size * sizeof(char));
}

void dstring_free(DynString *dstr) {//释放内存
	if (dstr) {
		if (dstr->data) {
			free(dstr->data);
		}
		dstr->cnt = dstr->cap = 0;
	}
}

void dstring_reset(DynString *dstr) {//重置：释放+初始化
	dstring_free(dstr);
	dstring_init(dstr, 8);
}

void dstring_realloc(DynString *dstr, int newsize) {//再分配空间（扩容）
	int cap=dstr->cap;
	char *data=dstr->data;
	while (cap < newsize) {
		cap *= 2;
	}
	data = realloc(data, cap*sizeof(char));
	if (!data) {
		error("内存分配失败");
	}
	dstr->cap = cap;
	dstr->data = data;
}

void dstring_getchar(DynString *dstr, char ch) {//输入新字符到串尾
	int cnt = dstr->cnt + 1;
	if (cnt > dstr->cap) {
		dstring_realloc(dstr, cnt);
	}
	dstr->data[cnt-1] = ch;
	dstr->cnt = cnt;
}