#include "compile.h"

FILE *fp;//文件指针
char *filename;//文件名

int main(int argc, char **argv) {
	printf(":****************************************************************:\n");
	printf(":********* 注：**************************************************:\n");
	printf(":********* 不支持自增运算符、自减运算符                 *********:\n");
	printf(":********* 不支持移位运算符、逻辑运算符、复合赋值运算符 *********:\n");
	printf(":****************************************************************:\n\n");
	/*fp = fopen(argv[1], "rb");
	if (argv[1][0] == '.'&&argv[1][1] == '/') {
		filename = argv[1] + 2;
	}
	else {
		filename = argv[1];
	}*/
	fp = fopen("test.c", "rb");
	filename = "test.c";
	if (!fp) {
		printf("无法打开源文件！\n");
		return 0;
	}
	init();
	lex_program();
	grammar_program();
	cleanup();
	fclose(fp);
	//_CrtDumpMemoryLeaks();
	system("pause");
	return 0;
}