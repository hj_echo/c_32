#include "compile.h"

//va_list是一组能解决变参问题的宏，通过vsprintf能够打印出规定信息
//fmt是参数输出的格式，ap是可变参数列表
//目前只考虑编译

DynArray print_warning;//保存编译警告的内容，在编译错误或者分析结束后再打印警告内容

void handle_exception(int stage, int level, char *fmt, va_list ap){//异常处理
	char buf[1024];
	vsprintf(buf, fmt, ap);
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(h, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);//打印颜色还原为白色
	if (stage == STAGE_COMPILE){
		if (level == LEVEL_WARNING) {
			char *s = (char*)malloc(strlen(filename) + strlen(buf) + 30);
			sprintf(s, "%s(第%d行): 编译警告: %s!\n", filename, line_num, buf);
			darray_pushback(&print_warning, s);
		}
		else{
			printf("\n");
			for (int i = 0; i < print_warning.cnt; i++) {
				printf("%s", (char*)print_warning.data[i]);
			}
			printf("%s(第%d行): 编译错误: %s!\n", filename, line_num, buf);
			cleanup();
			_CrtDumpMemoryLeaks();
			exit(-1);
		}
	}
	else{
		printf("链接错误: %s!\n", buf);
		cleanup();
		_CrtDumpMemoryLeaks();
		exit(-1);
	}
}

void warning(char *fmt, ...){//编译警告
	va_list ap = NULL;
	va_start(ap, fmt);
	handle_exception(STAGE_COMPILE, LEVEL_WARNING, fmt, ap);
	va_end(ap);
}

void error(char *fmt, ...){//编译错误
	va_list ap = NULL;
	va_start(ap, fmt);
	handle_exception(STAGE_COMPILE, LEVEL_ERROR, fmt, ap);
	va_end(ap);
}

void expect(char *msg){//缺少某个语法成分
	error("缺少%s", msg);
}