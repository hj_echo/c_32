#include "compile.h"

TkWord *tk_hashtable[MAXKEY];//单词哈希表
DynArray tk_table;//单词表

int elf_hash(char *key){//elf哈希函数
	int h = 0, g;
	while (*key)
	{
		h = (h << 4) + *key++;
		g = h & 0xf0000000;
		if (g)
			h ^= g >> 24;
		h &= ~g;
	}
	return h % MAXKEY;
}

TkWord* tkword_insert(TkWord *pword) {//直接在单词表中插入单词
	int key = elf_hash(pword->word);
	pword->next = tk_hashtable[key];
	tk_hashtable[key] = pword;
	darray_pushback(&tk_table, pword);
	return pword;
}

TkWord* tkword_find(char *word) {//在单词表（哈希表）中查找word是否存在
	int key = elf_hash(word);
	TkWord *p = tk_hashtable[key];
	while (p) {
		if (!strcmp(p->word, word)) {
			break;
		}
		p = p->next;
	}
	return p;
}

TkWord* tkword_find_insert(char *word) {//查找单词表中是否存在word，若不存在，则插入
	int key = elf_hash(word);
	TkWord *pword;
	pword = tkword_find(word);
	if (!pword) {
		pword = (TkWord*)malloc(sizeof(TkWord));
		pword->word = (char*)malloc((strlen(word) + 1)*sizeof(char));
		pword->next = tk_hashtable[key];
		tk_hashtable[key] = pword->next;
		darray_pushback(&tk_table, pword);
		pword->tkcode = tk_table.cnt - 1;
		strcpy(pword->word, word);
	}
	return pword;
}