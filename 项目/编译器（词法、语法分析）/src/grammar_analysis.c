#include "compile.h"


int syntax_state;  //语法状态
int syntax_level;  //缩进级别

int token_tag;//词法分析token序列的指针，即token序列表的下标
int const_tag;//词法分析常量序列的指针，即常量序列表的下标


/**********工具**********/
void gram_get_token() {//从token序列表中获取token值
	if (token_tag == token_seq.cnt) {
		error("内存访问越界");
	}
	token = *(int*)token_seq.data[token_tag++];
	if (token >= TK_CINT && token <= TK_CSTR) {
		dstring_reset(&sourcestr);
		char *s = const_seq.data[const_tag];
		for (int i = 0; i < strlen(s); i++) {
			dstring_getchar(&sourcestr, s[i]);
		}
		dstring_getchar(&sourcestr, '\0');
		const_tag++;
	}
	syntax_indent();
}

void skip(int v) {//跳过单词v，取下一个，如果不是v，则报错
	if (token != v) {
		expect(get_printword(v));
	}
	gram_get_token();
}

int is_type_specifier(int tk) {//判断是否是类型区分符
	switch (tk) {
	case KW_CHAR:
	case KW_SHORT:
	case KW_INT:
	case KW_DOUBLE:
	case KW_VOID:
	case KW_STRUCT:
		return 1;
	default:
		return 0;
	}
}
/************************/


/**********语法缩进**********/
void print_tab(int n) {//缩进n个tab键，因为不同机器的tab打印格式不同，统一为4个空格
	for (int i = 0; i < n; i++) {
		printf("    ");
	}
}

void syntax_indent() {//语法缩进主程序,根据当前的语法状态，先打印单词之前的space，再打印单词本身
	switch (syntax_state) {
	case SNTX_NUL:
		color_token();
		break;
	case SNTX_SP:
		printf(" ");
		color_token();
		break;
	case SNTX_LF_HT:
		if (token == TK_END) {// 遇到'}',缩进减少一级
			syntax_level--;
		}
		printf("\n");
		line_num++;
		print_line();
		print_tab(syntax_level);
		color_token();
		break;
	case SNTX_DELAY:
		break;
	}
	syntax_state = SNTX_NUL;
}
/****************************/


/**********外部定义**********/
void translation_unit() {//翻译单元，即推导的最外层
	while (token != TK_EOF) {
		external_declaration(SC_GLOBAL);
	}
}

void external_declaration(int storage_c) {//解析外部声明或者局部声明,storage_c表示存储类型
	if (!type_specifier()) {
		expect("<类型区分符>");
	}
	if (token == TK_SEMICOLON) {
		gram_get_token();
		return;
	}
	while (1) {//分析声明或者函数定义
		declarator();
		if (token == TK_BEGIN) {
			if (storage_c == SC_LOCAL) {//局部声明，不允许再定义函数
				error("不支持函数嵌套定义");
			}
			funcbody();
			break;
		}
		else {
			if (token == TK_ASSIGN) {//赋值号，局部变量声明并赋初值
				gram_get_token();
				initializer();
			}
			if (token == TK_COMMA) {//逗号，继续看下一个单词
				gram_get_token();
			}
			else {//终止符，声明结束
				syntax_state = SNTX_LF_HT;
				skip(TK_SEMICOLON);
				break;
			}
		}
	}
}

int type_specifier() {//解析类型区分符，返回是否找到
	int found_flag = 0;
	switch (token) {
	case KW_CHAR:
	case KW_SHORT:
	case KW_INT:
	case KW_DOUBLE:
	case KW_VOID:
		syntax_state = SNTX_SP;
		found_flag = 1;
		gram_get_token();
		break;
	case KW_STRUCT://单独处理结构体
		syntax_state = SNTX_SP;
		struct_specifier();
		found_flag = 1;
		break;
	default:
		break;
	}
	return found_flag;
}

void struct_specifier() {//解析结构体区分符，处理大括号
	int v;
	gram_get_token();
	v = token;
	syntax_state = SNTX_DELAY;
	gram_get_token();
	if (token == TK_BEGIN) {
		syntax_state = SNTX_LF_HT;
	}
	else if (token == TK_CLOSEPA) {
		syntax_state = SNTX_NUL;
	}
	else {
		syntax_state = SNTX_NUL;
	}
	syntax_indent();
	if (v < TK_IDENT) {//关键字不允许作为结构体的名称
		expect("结构体名");
	}
	if (token == TK_BEGIN) {
		struct_declaration_list();
	}
	//结构体声明可以不存在具体的内容表格（即大括号）
}

void struct_declaration_list() {//解析结构声明符表，处理大括号内部声明
	syntax_state = SNTX_LF_HT;//第一个结构体成员与'{'不在一行
	syntax_level++;//结构体变量声明，缩进增加一级
	gram_get_token();
	while (token != TK_END) {
		struct_declaration();
	}
	skip(TK_END);
	syntax_state = SNTX_LF_HT;
}

void struct_declaration() {//解析结构声明，识别声明
	if (!type_specifier()) {
		expect("<类型区分符>");
	}
	while (1) {
		declarator();
		if (token == TK_SEMICOLON) {
			break;
		}
		skip(TK_COMMA);
	}
	syntax_state = SNTX_LF_HT;
	skip(TK_SEMICOLON);
}

void function_calling_convention(int *fc) {//函数调用约定（详见函数调用规则）
	*fc = KW_CDECL;
	if (token == KW_CDECL || token == KW_STDCALL) {
		*fc = token;
		syntax_state = SNTX_SP;
		gram_get_token();
	}
}

void struct_member_alignment() {//结构体成员对齐
	if (token == KW_ALIGN) {
		gram_get_token();
		skip(TK_OPENPA);
		if (token == TK_CINT) {
			gram_get_token();
		}
		else {
			expect("整数常量");
		}
		skip(TK_CLOSEPA);
	}
}

void declarator() {//解析声明符，处理声明前的星号
	int fc;
	while (token == TK_STAR) {
		gram_get_token();
	}
	function_calling_convention(&fc);
	struct_member_alignment();
	direct_declarator();
}

void direct_declarator() {//解析直接声明符，识别标识符
	if (token >= TK_IDENT) {
		gram_get_token();
	}
	else {
		expect("<标识符>");
	}
	direct_declaration_postfix();
}

void direct_declaration_postfix() {//解析直接声明符后缀,处理方括号或者圆括号
	if (token == TK_OPENPA) {//圆括号
		parameter_type_list();
	}
	else if (token == TK_OPENBR) {//方括号
		gram_get_token();
		if (token == TK_CINT) {
			gram_get_token();
		}
		skip(TK_CLOSEBR);
		direct_declaration_postfix();
	}
}

void parameter_type_list() {//解析形参类型表,识别声明的标识符后括号内形参
	gram_get_token();
	while (token != TK_CLOSEPA) {//while外部还要判断的原因是括号内可以什么都没有
		if (!type_specifier()) {
			error("无效类型标识符");
		}
		declarator();
		if (token == TK_CLOSEPA) {
			break;
		}
		skip(TK_COMMA);
		if (token == TK_ELLIPSIS) {
			gram_get_token();
			break;
		}
	}
	syntax_state = SNTX_DELAY;
	skip(TK_CLOSEPA);
	if (token == TK_BEGIN) {//接下来是函数定义
		syntax_state = SNTX_LF_HT;
	}
	else {//接下来是函数声明
		syntax_state = SNTX_NUL;
	}
	syntax_indent();
}

void funcbody() {//解析函数体，处理复合语句
	//这里用一个壳包住的原因是方便语义分析时继续使用原来的架构
	compound_statement();
}

void initializer() {//解析初值符，处理赋值表达式
	//用壳包住的原因同上
	assignment_expression();
}
/****************************/


/**********语句**********/
void statement() {//解析语句，各类型语句解析的起点
	switch (token) {
	case TK_BEGIN:
		compound_statement();
		break;
	case KW_IF:
		if_statement();
		break;
	case KW_RETURN:
		return_statement();
		break;
	case KW_BREAK:
		break_statement();
		break;
	case KW_CONTINUE:
		continue_statement();
		break;
	case KW_FOR:
		for_statement();
		break;
	default://不是任何一种类型语句，则进入表达式语句
		expression_statement();
		break;
	}
}

void compound_statement() {//解析复合语句
	syntax_state = SNTX_LF_HT;
	syntax_level++;//复合语句，所以缩进增加一级
	gram_get_token();
	while (is_type_specifier(token)) {
		external_declaration(SC_LOCAL);
	}
	while (token != TK_END) {
		statement();
	}
	syntax_state = SNTX_DELAY;
	gram_get_token();
	if (token == KW_ELSE) {
		syntax_state = SNTX_SP;
	}
	else {
		syntax_state = SNTX_LF_HT;
	}
	syntax_indent();
}

void expression_statement() {//解析表达式语句
	if (token != TK_SEMICOLON) {
		expression();
	}
	syntax_state = SNTX_LF_HT;
	skip(TK_SEMICOLON);
}

void if_statement() {//解析if选择语句
	syntax_state = SNTX_SP;
	gram_get_token();
	skip(TK_OPENPA);
	expression();
	syntax_state = SNTX_LF_HT;
	skip(TK_CLOSEPA);
	statement();
	if (token == KW_ELSE) {
		syntax_state = SNTX_LF_HT;
		gram_get_token();
		statement();
	}
}

void for_statement() {//解析for循环语句
	gram_get_token();
	skip(TK_OPENPA);
	if (token != TK_SEMICOLON) {
		expression();
	}
	skip(TK_SEMICOLON);
	if (token != TK_SEMICOLON) {
		expression();
	}
	skip(TK_SEMICOLON);
	if (token != TK_SEMICOLON) {
		expression();
	}
	syntax_state = SNTX_LF_HT;
	skip(TK_CLOSEPA);
	statement();
}

void break_statement() {//解析break跳转语句
	gram_get_token();
	syntax_state = SNTX_LF_HT;
	skip(TK_SEMICOLON);
}

void continue_statement() {//解析continue跳转语句
	//两个跳转语句一模一样，其实还有一些特殊的错误情况这里识别不出来，需要语义分析加以剔除
	gram_get_token();
	syntax_state = SNTX_LF_HT;
	skip(TK_SEMICOLON);
}

void return_statement() {//解析return语句
	syntax_state = SNTX_DELAY;
	gram_get_token();
	if (token == TK_SEMICOLON) {
		syntax_state = SNTX_NUL;//用于return;
	}
	else {
		syntax_state = SNTX_SP;//用于return<expression>;
	}
	syntax_indent();
	if (token != TK_SEMICOLON) {
		expression();
	}
	syntax_state = SNTX_LF_HT;
	skip(TK_SEMICOLON);
}
/************************/


/**********表达式**********/
void expression() {//解析表达式，各类型表达式解析的起点
	while (1) {
		assignment_expression();
		if (token != TK_COMMA) {
			break;
		}
		gram_get_token();
	}
}

void assignment_expression() {//解析赋值表达式
	equality_expression();
	if (token == TK_ASSIGN) {
		gram_get_token();
		assignment_expression();
	}
}

void equality_expression() {//解析相等表达式
	relational_expression();
	while (token == TK_EQ || token == TK_NEQ) {
		gram_get_token();
		relational_expression();
	}
}

void relational_expression() {//解析关系表达式
	additive_expression();
	while (token == TK_LT || token == TK_LEQ || token == TK_GT || token == TK_GEQ) {
		gram_get_token();
		additive_expression();
	}
}

void additive_expression() {//解析加法表达式
	multiplicative_expression();
	while (token == TK_PLUS || token == TK_MINUS) {
		gram_get_token();
		multiplicative_expression();
	}
}

void multiplicative_expression() {//解析乘法表达式
	unary_expression();
	while (token == TK_STAR || token == TK_DIVIDE || token == TK_MOD) {
		gram_get_token();
		unary_expression();
	}
}

void unary_expression() {//解析一元表达式
	switch (token) {
	case TK_AND:
	case TK_STAR:
	case TK_PLUS:
	case TK_MINUS:
		gram_get_token();
		unary_expression();
		break;
	case KW_SIZEOF:
		sizeof_expression();
		break;
	default:
		postfix_expression();
		break;
	}
}

void sizeof_expression() {//解析sizeof表达式
	gram_get_token();
	skip(TK_OPENPA);
	type_specifier();
	skip(TK_CLOSEPA);
}

void postfix_expression() {//解析后缀表达式
	primary_expression();
	while (1) {
		if (token == TK_DOT || token == TK_POINTSTO) {//结构体成员
			gram_get_token();
			if (token < TK_IDENT) {
				error("错误的结构体成员名");
			}
			gram_get_token();
		}
		else if (token == TK_OPENBR) {//中括号
			gram_get_token();
			expression();
			skip(TK_CLOSEBR);
		}
		else if (token == TK_OPENPA) {//圆括号
			argument_expression_list();
		}
		else {
			break;
		}
	}
}

void primary_expression() {//解析初值表达式
	switch (token) {
	case TK_CINT:
	case TK_CDOUBLE:
	case TK_CCHAR:
	case TK_CSTR:
		gram_get_token();
		break;
	case TK_OPENPA:
		gram_get_token();
		expression();
		skip(TK_CLOSEPA);
		break;
	default:
		if (token < TK_IDENT) {
			expect("标识符或常量");
		}
		gram_get_token();
		break;
	}
}

void argument_expression_list() {//解析实参表达式
	gram_get_token();
	if (token != TK_CLOSEPA) {
		while (1) {
			assignment_expression();
			if (token == TK_CLOSEPA) {
				break;
			}
			skip(TK_COMMA);
		}
	}
	skip(TK_CLOSEPA);
}
/**************************/