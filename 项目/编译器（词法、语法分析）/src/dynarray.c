#include "compile.h"

void darray_init(DynArray *darr, int size) {//初始化
	darr->cnt = 0;
	darr->cap = size;
	darr->data = (void**)malloc(size * sizeof(void*));
}

void darray_free(DynArray *darr) {//释放内存
	for (int i = 0; i < darr->cnt; i++) {
		if (darr->data[i]) {
			free(darr->data[i]);
		}
	}
	free(darr->data);
	darr->data = NULL;
	darr->cnt = darr->cap = 0;
}

void darray_reset(DynArray *darr) {//重置：释放+初始化
	darray_free(darr);
	darray_init(darr, 50);
}

void darray_realloc(DynArray *darr, int newsize) {//再分配内存空间（扩容）
	int cap = darr->cap;
	void **data = darr->data;
	while (cap < newsize) {
		cap *= 2;
	}
	data = realloc(data, cap*sizeof(void*));
	if (!data) {
		error("内存分配失败");
	}
	darr->cap = cap;
	darr->data = data;
}

void darray_pushback(DynArray *darr, void* data) {//在数组尾插入新的元素
	int cnt = darr->cnt+1;
	if (cnt > darr->cap) {
		darray_realloc(darr, cnt);
	}
	darr->data[cnt - 1] = data;
	darr->cnt = cnt;
}
