#include "compile.h"

void getch() {//从源码中读取一个字符
	ch = fgetc(fp);
}

void print_line() {
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(h, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	printf("%3d: ",line_num);
}

char* get_printword(int tkcode) {
	if (tkcode > tk_table.cnt) {
		return NULL;
	}
	else if (tkcode >= TK_CINT && tkcode <= TK_CSTR) {
		return sourcestr.data;
	}
	else {
		return ((TkWord*)tk_table.data[tkcode])->word;
	}
}

void color_token() {
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	char * p;
	if (token >= TK_IDENT) {//标识符为灰色
		SetConsoleTextAttribute(h, FOREGROUND_INTENSITY);
	}
	else if (token >= KW_CHAR) {//关键字为绿色 
		SetConsoleTextAttribute(h, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	}
	else if (token >= TK_CINT) {//常量为黄色
		SetConsoleTextAttribute(h, FOREGROUND_RED | FOREGROUND_GREEN);
	}
	else {//运算符及分隔符为红色
		SetConsoleTextAttribute(h, FOREGROUND_RED | FOREGROUND_INTENSITY);
	}
	p = get_printword(token);
	printf("%s", p);
}

void init() {//初始化程序
	line_num = 1;
	darray_init(&print_warning, 8);
	lex_anaylysis_init();
	token_tag = 0;
	const_tag = 0;
}

void cleanup() {//清理程序
	/*清理单词表*/
	for (int i = TK_IDENT; i < tk_table.cnt; i++) {
		//必须从TK_IDENT开始释放，因为之前的单词是放在静态存储区（keywords）中的，不是在堆中
		free(((TkWord*)tk_table.data[i])->word);
		free(tk_table.data[i]);
	}
	free(tk_table.data);
	/*清理编译警告表*/
	for (int i = 0; i < print_warning.cnt; i++) {
		free(print_warning.data[i]);
	}
	free(print_warning.data);
	/*清理token序列表*/
	for (int i = 0; i < token_seq.cnt; i++) {
		free(token_seq.data[i]);
	}
	free(token_seq.data);
	/*清理常量序列表*/
	for (int i = 0; i < const_seq.cnt; i++) {
		free(const_seq.data[i]);
	}
	free(const_seq.data);
	/*清理动态字符串*/
	free(str.data);
	free(sourcestr.data);
}

void lex_program() {
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(h, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	printf("词法分析开始！\n");
	printf("\n打印源码（忽略注释）：\n");
	print_line();
	getch();
	do {
		get_token();
		color_token();
	} while (token != TK_EOF);
	SetConsoleTextAttribute(h, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);//打印颜色还原为白色
	printf("\n\n");
	for (int i = 0; i < print_warning.cnt; i++) {
		printf("%s", (char*)print_warning.data[i]);
	}
	printf("代码行数：%d行\n", line_num);
	printf("%s 词法分析成功！\n", filename);
}

void grammar_program() {
	printf("\n\n语法分析开始！\n");
	darray_reset(&print_warning);
	line_num = 1;
	printf("\n打印格式化后代码：\n");
	print_line();
	gram_get_token();
	translation_unit();
	printf("\n\n");
	HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(h, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	for (int i = 0; i < print_warning.cnt; i++) {
		printf("%s", (char*)print_warning.data[i]);
	}
	printf("代码行数：%d行\n", line_num);
	printf("%s 语法分析成功！\n", filename);
}